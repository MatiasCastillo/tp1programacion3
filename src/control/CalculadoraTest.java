package control;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CalculadoraTest {
	String numero1="1";
	String numero2="1";
	String decimal1="1.1";
	String decimal2="2.2";
	String resultado="0";
	
	@Before
	public void reset(){
		resultado="0";
	}
	
	@Test
	public void sumarTest() {	
		resultado=Funciones.suma(numero1, numero2);
		assertEquals("2",resultado);
	}
	
	@Test
	public void restarTest(){
		resultado=Funciones.resta(numero1, numero2);
		assertEquals("0",resultado);
	}
	
	@Test	
	public void multiplicarTest(){
		resultado=Funciones.multiplicacion(numero1, numero2);
		assertEquals("1",resultado);
	}
	
	@Test
	public void dividirTest(){
		resultado=Funciones.division(numero1, numero2);
		assertEquals("1",resultado);
	}
	
	@Test
	public void borrarUnDigitoTest(){
		assertEquals("",Funciones.borrarUnDigito(numero1));
	}
	
	public void decimalTest(){
		assertEquals("1.",Funciones.agregarPuntoDecimal(numero1));
	}
	
	@Test
	public void sumarDecimalesTest() {	
		resultado=Funciones.suma(decimal1, decimal2);
		assertEquals("3.3",resultado);
	}
		
	@Test
	public void divisionPorCeroTest() {
		resultado=Funciones.division(numero1, "0");
		assertEquals("Imposible dividir por 0",resultado);
	}
}
