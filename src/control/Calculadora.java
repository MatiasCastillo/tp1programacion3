package control;

public class Calculadora {
	private char operacion;
	private String numero;
	private boolean flagResultado;
	private boolean flagGuardarMemoria;
	private boolean flagUsarMemoria;
	private boolean flagOperacion;
	private String[] numerosEnMemoria;
	
	public Calculadora() {
		numero="0";
		flagResultado=false;
		flagGuardarMemoria=false;
		flagUsarMemoria=false;
		numerosEnMemoria=new String[10];
	}

	public char getOp() {
		return operacion;
	}

	public void setOp(char op) {
		this.operacion = op;
	}

	public boolean isFlagOperacion() {
		return flagOperacion;
	}

	public void setFlagOperacion(boolean flagOperacion) {
		this.flagOperacion = flagOperacion;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public boolean isFlagResultado() {
		return flagResultado;
	}

	public void setFlagResultado(boolean flagResultado) {
		this.flagResultado = flagResultado;
	}

	public boolean isFlagGuardarMemoria() {
		return flagGuardarMemoria;
	}

	public void setFlagGuardarMemoria(boolean flagGuardarMemoria) {
		this.flagGuardarMemoria = flagGuardarMemoria;
	}

	public boolean isFlagUsarMemoria() {
		return flagUsarMemoria;
	}

	public void setFlagUsarMemoria(boolean flagUsarMemoria) {
		this.flagUsarMemoria = flagUsarMemoria;
	}

	public String[] getNumerosEnMemoria() {
		return numerosEnMemoria;
	}

	public void setNumerosEnMemoria(String[] numerosEnMemoria) {
		this.numerosEnMemoria = numerosEnMemoria;
	}
	
	public String comportamientoNumeroIngresado(String num, char num_ingresado){
		return Funciones.comportamientoNumeroIngresado(this,num, num_ingresado);
	}

	public String guardarNum_Operacion(String numero1, char c) {
		if(!numero1.equals("")) {
			numero=Funciones.guardarNumero(numero1);
			operacion=c;
			flagOperacion=true;
			return "";
		}
			return "";
	}

	public String mostrarResultado(String s) {
		String aux =Funciones.resolver(operacion,numero,s); 
		numero=Funciones.guardarNumero(s);
		flagResultado=false;
		operacion=' ';
		return aux;
	}

	public String mostrarCuenta(String s) {
		return Funciones.mostrarCuenta(numero,operacion,s);
	}

	public void limpiarTodo() {
		numero="0";
		flagResultado=false;
		flagGuardarMemoria=false;
		flagUsarMemoria=false;
		numerosEnMemoria=new String[10];
	}
	
	public void limpiarMemoria(){
		numerosEnMemoria=new String[10];
	}
}
