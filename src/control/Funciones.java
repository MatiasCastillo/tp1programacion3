package control;

import java.math.BigDecimal;

public class Funciones {
	static BigDecimal x;
	static BigDecimal y;
	static String[] operadores={"+","-","*","/"};

	public static String resolver(char operador,String i,String j){
		if(operador==' '){
			return i;
		}
		if(operador=='-'){
			return resta(i,j);
		}
		else if(operador=='+'){
			return suma(i,j);
		}
		else if(operador=='*'){
			return multiplicacion(i,j);
		}
		else if(operador=='/'){
			return division(i,j);
		}
		return "Error";
	}

	public static String suma (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.add(y);
		return String.valueOf(resultado);
		
	}
	
	public static String resta (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.subtract(y);
		return String.valueOf(resultado);
	}
	
	public static String multiplicacion (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.multiply(y);
		return String.valueOf(resultado);
	}
	
	public static String division (String i , String j){
				
		if(j.equals("0")) {
			return "Imposible dividir por 0";
		}
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.divide(y);
		return String.valueOf(resultado);
	}
	
	public static String guardarNumero(String numero){
		if(numero!=""){
			return numero;
		}
		else{
			return "Ingrese un numero";
		}
	}
	
	public static String borrarUnDigito(String numero){
		String numero_aux=numero;
		if (numero_aux != "" && numero_aux.length() > 0 ) {
		      numero_aux = numero_aux.substring(0, numero_aux.length()-1);
		    }
		    return numero_aux;
	}
	
	public static String concatenar(String numero, char numero_ingresado){
			return numero+numero_ingresado;
	}
	
	public static String agregarPuntoDecimal(String numero){
		String numero_aux=numero;
		boolean entero=true;
		if(numero_aux.contains(".")){
			entero= false;
		}
		else if(entero&&!numero_aux.equals("")){
			numero_aux=numero_aux+'.';
		}
		return numero_aux;
	}
	
	public static String mostrarCuenta(String operando1,char operador,String operador2){
		return operando1+operador+operador2;
	}
	
	public static String deshacerUltimaCuenta(String cuenta){
		String numero="";
		boolean check=true;
		if(!cuenta.equals(""))
			for(int i=0; i<cuenta.length();i++){
				if(check){
					if(cuenta.charAt(i)=='+'||cuenta.charAt(i)=='-'||cuenta.charAt(i)=='*'||cuenta.charAt(i)=='/'){
						check=false;
					}
					else{
						numero=numero+cuenta.charAt(i);
					}
				}
			}
			
		return numero;
	}
	
	public static String comportamientoNumeroIngresado(Calculadora cal, String numero,char numero_ingresado){
		String[] lista= cal.getNumerosEnMemoria();
		if(numero.equals("0")&&numero_ingresado=='0'){//no agrega mas que un solo 0 si no hay nada mas
			return numero;
		}
		if(cal.isFlagGuardarMemoria()){
			lista[Character.getNumericValue(numero_ingresado)]=numero;
			cal.setNumerosEnMemoria(lista);
			cal.setFlagGuardarMemoria(false);
			return "";
		}		
		else if(cal.isFlagUsarMemoria()){
			cal.setFlagUsarMemoria(false);
			return cal.getNumerosEnMemoria()[Character.getNumericValue(numero_ingresado)];
		}
		else{
			if(cal.isFlagResultado()){	
					return Funciones.concatenar(numero,numero_ingresado);
				}else{
					numero="";
					cal.setFlagResultado(true);
					return Funciones.concatenar(numero,numero_ingresado);
				}
			}
		}
}