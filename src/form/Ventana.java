package form;

import control.Calculadora;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import java.awt.Dimension;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import control.Funciones;

public class Ventana {
	private JFrame frame;
	private JTextField _display;
	private JTextField _display2;
	private Calculadora c;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana window = new Ventana();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public Ventana() {
		initialize();
		c=new Calculadora();
	}
	
	private void initialize() {
		frame = new JFrame();
		frame.setResizable(false);
		frame.getContentPane().setBackground(new Color(128, 128, 128));
		frame.getContentPane().setMaximumSize(new Dimension(45, 35));
		frame.setBounds(100, 100, 244, 434);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		//--------------------------
		JButton _1 = new JButton("1");
		_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '1'));			
			}
		});
		
		JLabel _displayM = new JLabel("M");
		_displayM.setForeground(Color.WHITE);
		_displayM.setFont(new Font("Tahoma", Font.BOLD, 12));
		_displayM.setBounds(19, 11, 46, 15);
		frame.getContentPane().add(_displayM);
		_1.setBounds(10, 304, 55, 40);
		frame.getContentPane().add(_1);
		//---------------------------
		JButton _2 = new JButton("2");
		_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '2'));
			}
		});
		_2.setBounds(65, 304, 55, 40);
		frame.getContentPane().add(_2);
		//-----------------------------
		JButton _3 = new JButton("3");
		_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '3'));
			}
		});
		_3.setBounds(120, 304, 55, 40);
		frame.getContentPane().add(_3);
		//-----------------------------
		JButton _4 = new JButton("4");
		_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '4'));
			}	
		});
		_4.setBounds(10, 253, 55, 40);
		frame.getContentPane().add(_4);
		//----------------------------
		JButton _5 = new JButton("5");
		_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '5'));
			}
		});
		_5.setBounds(65, 253, 55, 40);
		frame.getContentPane().add(_5);
		//-----------------------------
		JButton _6 = new JButton("6");
		_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '6'));
			}
		});
		_6.setBounds(120, 253, 55, 40);
		frame.getContentPane().add(_6);
		JButton _div = new JButton("\u00F7");
		_div.setToolTipText("Dividir");
		//------------------------------
		JButton _7 = new JButton("7");
		_7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '7'));
			}
		});
		_7.setBounds(10, 202, 55, 40);
		frame.getContentPane().add(_7);
		//----------------------------
		JButton _8 = new JButton("8");
		_8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '8'));
			}
		});
		_8.setBounds(65, 202, 55, 40);
		frame.getContentPane().add(_8);
		//----------------------------
		JButton _9 = new JButton("9");
		_9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '9'));
			}
		});
		_9.setBounds(120, 202, 55, 40);
		frame.getContentPane().add(_9);
		//-----------------------------
		JButton _0 = new JButton("0");
		_0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.comportamientoNumeroIngresado(_display.getText(), '0'));
			}
		});
		_0.setBounds(10, 352, 55, 40);
		frame.getContentPane().add(_0);
		//-----------------------------
		_div.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.guardarNum_Operacion(_display.getText(),'/'));
			}
		});
		_div.setBounds(175, 151, 55, 40);
		frame.getContentPane().add(_div);
		//-----------------------------
		JButton _resta = new JButton("-");
		_resta.setToolTipText("Restar");
		_resta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.guardarNum_Operacion(_display.getText(),'-'));
			}
		});
		_resta.setBounds(175, 253, 55, 40);
		frame.getContentPane().add(_resta);
		//--------------------------------
		JButton _multi = new JButton("x");
		_multi.setToolTipText("Multiplicar");
		_multi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.guardarNum_Operacion(_display.getText(),'*'));
			}
		});
		_multi.setBounds(175, 202, 55, 40);
		frame.getContentPane().add(_multi);
		//---------------------------------
		JButton _suma = new JButton("+");
		_suma.setToolTipText("Sumar");
		_suma.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText(c.guardarNum_Operacion(_display.getText(),'+'));

			}
		});
		_suma.setBounds(175, 304, 55, 88);
		frame.getContentPane().add(_suma);
		//--------------------------------
		JButton _borrar = new JButton("<--");
		_borrar.setToolTipText("Borrar \u00F9ltimo caracter");
		_borrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.borrarUnDigito(_display.getText()));
			}
		});
		_borrar.setFont(new Font("Tahoma", Font.PLAIN, 10));
		_borrar.setBounds(120, 151, 55, 40);
		frame.getContentPane().add(_borrar);
		//--------------------------------
		JButton memory_storage = new JButton("MS");
		memory_storage.setToolTipText("Memory Storage");
		memory_storage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				c.setFlagGuardarMemoria(true);
			}
		});
		memory_storage.setBounds(10, 100, 55, 40);
		frame.getContentPane().add(memory_storage);
		//--------------------------------
		JButton _reiniciar = new JButton("C");
		_reiniciar.setToolTipText("Borrar pantalla");
		_reiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText("0");
				_display2.setText("");
			}
		});
		_reiniciar.setBounds(65, 151, 55, 40);
		frame.getContentPane().add(_reiniciar);
		//-----------------------------------------
		JButton _dec = new JButton(".");
		_dec.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.agregarPuntoDecimal(_display.getText()));
			}
		});
		_dec.setBounds(65, 352, 55, 40);
		frame.getContentPane().add(_dec);
		//-----------------------------------------
		JButton _igual = new JButton("=");
		_igual.setToolTipText("Obtener resultado");
		_igual.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(c.isFlagResultado()&&c.isFlagOperacion()){
					_display2.setText(c.mostrarCuenta(_display.getText()));
					_display.setText(c.mostrarResultado(_display.getText()));
					c.setFlagOperacion(false);
				}		
			}
		});
		_igual.setBounds(120, 352, 55, 40);
		frame.getContentPane().add(_igual);
		
		
		_display = new JTextField();
		_display.setEditable(false);
		_display.setText("0");
		_display.setFont(new Font("Nirmala UI", Font.BOLD, 18));
		_display.setHorizontalAlignment(SwingConstants.RIGHT);
		_display.setForeground(Color.WHITE);
		_display.setBackground(Color.BLACK);
		_display.setBounds(10, 57, 220, 35);
		frame.getContentPane().add(_display);
		_display.setColumns(10);
		_display2 = new JTextField();
		_display2.setEnabled(false);
		_display2.setEditable(false);
		_display2.setFont(new Font("Nirmala UI", Font.PLAIN, 18));
		_display2.setHorizontalAlignment(SwingConstants.RIGHT);
		_display2.setForeground(Color.WHITE);
		_display2.setBackground(Color.BLACK);
		_display2.setBounds(10, 11, 220, 35);
		frame.getContentPane().add(_display2);
		_display2.setColumns(10);
		
		
		JButton _deshacer = new JButton("Des.");
		_deshacer.setToolTipText("Deshacer");
		_deshacer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				_display.setText(Funciones.deshacerUltimaCuenta(_display2.getText()));
				_display2.setText("");
			}
		});
		_deshacer.setBounds(175, 100, 55, 40);
		frame.getContentPane().add(_deshacer);
		
		JButton btnAc = new JButton("AC");
		btnAc.setToolTipText("Borrar pantalla y memoria");
		btnAc.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				_display.setText("0");
				_display2.setText("");
				c.limpiarTodo();
			}
		});
		btnAc.setFont(new Font("Tahoma", Font.PLAIN, 11));
		btnAc.setBounds(10, 151, 55, 40);
		frame.getContentPane().add(btnAc);
		
		JButton memory_clear = new JButton("MC");
		memory_clear.setToolTipText("Memory Cleaner");
		memory_clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.limpiarMemoria();
			}
		});
		
		memory_clear.setBounds(65, 100, 55, 40);
		frame.getContentPane().add(memory_clear);		
		JButton memory_recall = new JButton("MR");
		memory_recall.setToolTipText("Memory Recall");
		memory_recall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				c.setFlagUsarMemoria(true);
			}
		});
		memory_recall.setBounds(120, 100, 55, 40);
		frame.getContentPane().add(memory_recall);
	}
}