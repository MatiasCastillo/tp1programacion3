= Programación III: Trabajo Práctico Calculadora

Brignole Federico <federicobrignole@hotmail.com>

Castillo Alvaro Matias <amatiascastillo@gmail.com>

-UNIVERSIDAD NACIONAL GENERAL SARMIENTO


{docdate}. 
Profesores: Alejandro Nelis  y Fernando Torres (COM-02)

:numbered:
:source-highlighter: highlight.js
:tabsize: 4

== Introducción

En el siguiente trabajo se desarrollo a pedido de la cátedra una calculadora basica, la cual suma, resta, multiplica, divide, tiene 10 posiciones para almacenar números en memoria y cuenta con 2 pantallas, la pantalla superior es la que va a mostrar la cuenta realizada, y la inferior, irá mostrando los números que se van ingresando y el resultado. 

== Descripción

El programa consta de 3 clases: *Funciones.class* , *Calculadora.class* , *Ventana.class*;

*Funciones.class:*
     
En esta clase se encuentra la parte lógica del programa, aquí se realizaran las operaciones matemáticas (suma, resta, etc), asignación del punto decimal, acción de borrar digito por digito y verificar si al momento de ingresar un número se desea guardarlo en memoria, buscar un número previamente guardado en memoria o simplemente escribir en la pantalla de la calculadora para posteriormente realizar una operación.

*Calculadora.class:*
     
La clase calculadora es la que comunica la parte lógica con la parte visual, aquí es donde se utilizan los métodos de la clase *Funciones*, estos métodos dependen de las siguentes variables de estado:

_flagResultado:_ Se encarga de comprobar si hay o no un resultado en la pantalla inferior, optamos por esta variable para solucionar el problema de que cuando se muestra el resultado de una operación y se quiere seguir con otra, no se siga escribiendo los números a continuación, si no que primero se limpia la pantalla y luego se muestra el numero ingresado, de lo contrario (si no hay un resultado en pantalla), se concatenan los números ingresados.

_flagGuardarMemoria:_ Al oprimir el botón "MS"(Memory Storage) esta variable cambia su estado, por lo que almacena el número en pantalla y pide otro numero que es en la posición de la memoria en la cual se va a guardar el numero previamente ingresado.

_flagUsarMemoria:_ Este estado se activa cuando se oprime el botón "MR"(Memory Recall), cuando está en estado "true", y a continuación se oprime un numero se muestra el numero guardado en esa posición de memoria.

_numerosEnMemoria:_ Esta lista de 10 posiciones es la encargada de almacenar los numero en la memoria de la calculadora.

*Ventana.class:*

En esta clase se encuentra la ventana realizada en Windows Builder.

== Implementación

Para realizarse las operaciones principales se utiliza el siguiente método:

*Clase: Funciones*  
_Metodo Resolver:_

[source, java]
----
public static String resolver(char operador,String i,String j){
		if(operador==' '){
			return i;
		}
		if(operador=='-'){
			return resta(i,j);
		}
		else if(operador=='+'){
			return suma(i,j);
		}
		else if(operador=='*'){
			return multiplicacion(i,j);
		}
		else if(operador=='/'){
			return division(i,j);
		}
		return "Error";
	}
----
El método "resolver" solicita tres argumentos, un carácter, en el cual se verá representada la operación que se quiere realizar, y dos cadenas de caracteres, las cuales serán los números a operar. Para procesar los datos, llama a la función correspondiente al carácter ingresado, las funciones son las siguientes:

_Metodo Suma:_
[source, java]
----
public static String suma (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.add(y);
		return String.valueOf(resultado);
		
	}	
----

_Metodo Resta:_
[source, java]
----
public static String resta (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.subtract(y);
		return String.valueOf(resultado);
	}
----

_Metodo Multiplicación:_
[source, java]
----
public static String multiplicacion (String i , String j){
		
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.multiply(y);
		return String.valueOf(resultado);
	}
----

_Metodo División:_
[source, java]
----
public static String division (String i , String j){
				
		if(j.equals("0")) {
			return "Imposible dividir por cero";
	    	}
		x = new BigDecimal(i);
		y = new BigDecimal(j);
		BigDecimal resultado = x.divide(y);
		return String.valueOf(resultado);
	}
----
La funcionalidad de los métodos antes mencionados, es similar en todos ellos, excepto en la división, que se tiene en cuenta la imposibilidad matemática de dividir por 0(Cero). Cada método solicita dos argumentos que son los números con los cuales va a realizar la operación.
Al ejecutar los test unitarios con operaciones decimales no se recibía el resultado esperado, por esta razón fue que se implementó BigDecimal.

Al momento de ingresar los numero la siguiente función es la encargada de tomar la decisión de que hacer dependiendo de lo que se haya o no hecho previamente:

_Metodo Comportamiento Numero Ingresado:_
[source, java]
----
public static String comportamientoNumeroIngresado(Calculadora cal, String numero,char numero_ingresado){
		String[] lista= cal.getNumerosEnMemoria();
		if(numero.equals("0")&&numero_ingresado=='0'){//no agrega mas que un solo 0 si no hay nada mas
			return numero;
		}
		if(cal.isFlagGuardarMemoria()){
			lista[Character.getNumericValue(numero_ingresado)]=numero;
			cal.setNumerosEnMemoria(lista);
			cal.setFlagGuardarMemoria(false);
			return "";
		}		
		else if(cal.isFlagUsarMemoria()){
			cal.setFlagUsarMemoria(false);
			return cal.getNumerosEnMemoria()[Character.getNumericValue(numero_ingresado)];
		}
		else{
			if(cal.isFlagResultado()){	
					return Funciones.concatenar(numero,numero_ingresado);
				}else{
					numero="";
					cal.setFlagResultado(true);
					return Funciones.concatenar(numero,numero_ingresado);
				}
			}
		}
----
Si _isFlagGuardarMemoria()_ es verdadero, se pretende que ya haya un numero ingresado en la pantalla, porque se solicita un número del cero al nueve que será la posición de memoria a donde se almacenará el numero ingresado con anterioridad.
En cambio, si _isFlagUsarMemoria()_ da como resultado verdadero, si ya había un número del 0 al 9 ingresado se  mostrara en pantalla el número correspondiente a esa posción de memoria.
Por el contrario, si ninguna de las variables anteriores son verdades, se comprobará si en la pantalla se encuentra un resultado o no, para sobrescribirlo  o para escribir a continuación del numero ingresado así llegar a el número que se desea calcular.

Para poder agregar el punto decimal al número, se implementó la siguiente función:

_Metodo Agregar Punto Decimal_

[source, java]
----
public static String agregarPuntoDecimal(String numero){
		String numero_aux=numero;
		boolean entero=true;
		if(numero_aux.contains(".")){
			entero= false;
		}
		else if(entero&&!numero_aux.equals("")){
			numero_aux=numero_aux+'.';
		}
		return numero_aux;
	}
----

Este método permite agregar el punto teniendo en cuenta que no se ponga un punto si no se antepone un número, ni que se agregue más de un punto decimal.


Una vez realizada la operación requerida se tiene la opción de deshacer la última acción, esto re realiza con el siguiente método:

_Metodo Deshacer_
[source, java]
----
public static String deshacerUltimaCuenta(String cuenta){
		String numero="";
		boolean check=true;
		if(!cuenta.equals(""))
			for(int i=0; i<cuenta.length();i++){
				if(check){
					if(cuenta.charAt(i)=='+'||cuenta.charAt(i)=='-'||cuenta.charAt(i)=='*'||cuenta.charAt(i)=='/'){
						check=false;
					}
					else{
						numero=numero+cuenta.charAt(i);
					}
				}
			}
			
		return numero;
	}
----
Cuando en la pantalla inferior se muestra el resultado, en la superior se puede observar la cuenta realizada, la función deshacer recibe la información almacenada en la pantalla superior. Dicha información es la utilizada para deshacer la cuenta. 

== Conclusiones

La resolución del trabajo practico desde el punto de vista teórico no parecía tener dificultades, solo había que aprender a utilizar lo que ya sabíamos en Windows Builder y acomodar nuestro código tal como era requerido, tiendo en cuenta la arquitectura "Forms and Control", y aquí fue a donde se presentaron las primeras dificultades, ya que en un primer momento, habíamos colocado las variables de estado en la clase Ventana, así también, dicha clase tenía "getters y setters" que se comunicaban con la clase Funciones para realizar algunas acciones. La manera de solucionar este problema fue creando otra clase que se encargue que los estados y de utilizar los métodos de la clase Funciones, para posteriormente ser utilizada en la Ventana, así creamos la clase Calculadora, dejando así a la Ventana solo con los elementos visuales y las llamadas a los métodos de Calculadora.
